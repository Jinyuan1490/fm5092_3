﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_A1
{
    //We creat an options class, and will use it as a parent class for all kinds of options.
    public class Options
    {
        //We difined several property of the objects in the option class.
        //Isput indicates that if this objects is put option or call option.
        public bool isput = false;
        //S0 is the underlying price, K is the strike price, r is the risk free rate, sigma is volatility, T is tenor.
        public double S0, K, r, sigma, T;
        //In order to calculate greek values, I set the random variables matrix R as a property for now. 
        //Actually I don't think this is a good idea, and I will find some way to improve this.
        public double[,] R1;
        //This method is used to get the results of the Simulator.Simulate method.
        public double[,] Getsim(int steps, int N)
        {
            double[,] sims;
            sims = Simulator.Simulate(S0, sigma, r, T, steps, N, R1);
            return sims;
        }
        public double[,] Getsim2(int steps, int N)
        {
            double[,] sims2;
            double[,] R2 = new double[N, steps + 1];
            int i, j;
            for (j = 0; j < N; j++)
            {
                for (i = 1; i <= steps; i++)
                {
                    R2[j, i - 1] = -R1[j, i - 1];
                }
            }
            sims2 = Simulator.Simulate(S0, sigma, r, T, steps, N, R2);
            return sims2;
        }
    }

    //We created another class named EuroOption, which inherited the parent class Options.
    public class EuroOption : Options
    {
        //This method is used to calculate the European option price.
        public double PriceEuro(int steps, int N, Boolean IsAnti, Boolean IsCV)
        {
            double sum = 0, price = 0;
            int i;
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N);
            if (IsAnti == false && IsCV == false)
            {
                for (i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        //Gives the option price of call option.
                        sum += Math.Max(sims[i, steps] - K, 0);
                    }
                    else
                    {
                        //Gives the option price of put option.
                        sum += Math.Max(K - sims[i, steps], 0);
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N);
                for (i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sum += Math.Max(sims[i, steps] - K, 0);
                        sum += Math.Max(sims2[i, steps] - K, 0);
                    }
                    else
                    {
                        sum += Math.Max(K - sims[i, steps], 0);
                        sum += Math.Max(K - sims2[i, steps], 0);
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double t, delta;
                int j;//j steps.
                for (i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(St - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-St + K, 0) + beta1 * cv;
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);                
            }
            else if(IsAnti== true && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;
                int j;
                for (i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1 + Math.Max(0, St2 - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -St1 + K) + beta1 * cv1 + Math.Max(0, -St2 + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            return price;
        }
        //This method is used to calculate the standard error.
        public double Std(int steps, int N, Boolean IsAnti, Boolean IsCV)
        {
            double[,] sims;
            sims = Getsim(steps, N);
            int i;
            double sums = 0, Std=0;
            double price = PriceEuro(steps, N, IsAnti,IsCV);
            if (IsAnti == false && IsCV==false)
            {
                for (i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow((Math.Max(sims[i, steps] - K, 0) * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow((Math.Max(K - sims[i, steps], 0) * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if(IsAnti==true && IsCV==false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N);
                for (i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow(((Math.Max(sims[i, steps] - K, 0) + Math.Max(sims2[i, steps] - K, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow(((Math.Max(K - sims[i, steps], 0) + Math.Max(K - sims2[i, steps], 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv,St,Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double t, delta;
                int j;//j steps.
                for (i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(St - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-St + K, 0) + beta1 * cv;
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;
                int j;

                for (i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;                      
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1 + Math.Max(0, St2 - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, - St1 + K) + beta1 * cv1 + Math.Max(0, - St2 + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }
            return Std;
        }
    }


}
